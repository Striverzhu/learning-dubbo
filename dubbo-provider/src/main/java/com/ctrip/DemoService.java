package com.ctrip;

/**
 * Created by xingyuzhu on 2017/10/11.
 */
public interface DemoService {
    String sayHello(String name);
}
