package com.ctrip;

import org.springframework.stereotype.Component;

/**
 * Created by xingyuzhu on 2017/10/11.
 */
@Component
public class DemoServiceImpl implements DemoService {
    public String sayHello(String name) {
        return "Hello " + name;
    }
}
