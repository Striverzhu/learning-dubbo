package com.ctrip;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by xingyuzhu on 2017/10/11.
 */

public class Consumer {
    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
                new String[]{"dubbo-demo-consumer.xml"});
        context.start();
        DemoService demoService = (DemoService) context.getBean("demoService"); // obtain proxy object for remote invocation
        String hello = demoService.sayHello("world, zhuxingyu succeed!"); // execute remote invocation
        System.out.println(hello); // show the result
    }
}
